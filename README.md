# Discovering Hydrographic Parameters of HydroSHEDS and TanDEM-X Digital Elevation Model through Exploratory Data Analysis

Climate change and anthropogenic factors, including groundwater pumping, deforestation, urbanization, and agriculture, impact the fluvial systems and cause degradation of the ecosystem. Earth Observation (EO) offers effective mitigation strategies to assess the impact on the fluvial systems of diverse regions at various spatial resolutions. In Hydrology, topographic catchments are considered a fundamental element for representing a standard unit of measure and are leveraged for hydrological analysis. The hydro-environmental features of the catchments are derived from the delineation of river networks and watershed boundaries by utilization of digital elevation models (DEMs). This study adopted the catchment geometries and features from an open-source, multi-scale hydro-environmental dataset termed HydroSHEDS. It offers uniform regional and global scale hydrographic information in a comprehensive format, including raster and vector formats. This study demonstrates the development and incorporation of geomorphometric parameters, including the Topographic Wetness Index (TWI), Terrain Ruggedness Index (TRI), and Geomorphons from the SRTM (Shuttle Radar Topography Mission) and TanDEM-X (TerraSAR-X add-on for Digital Elevation Measurement) digital elevation models (DEM) into HydroSHEDS dataset by leveraging a hybrid workflow of QGIS (Quantum Geographical Information System) and Python at a catchment scale. Additionally, the relationship of geomorphometric parameters with multi-annual environmental variables, including Soil Moisture, Precipitation Height, and Drought Index obtained from DWD (Deutscher Wetterdienst) and various hydro-environmental features from HydroSHEDS based on environmental variables is investigated utilizing the Exploratory Data Analysis (EDA) techniques such as Descriptive Statistics, Correlation, Multiple Regression, and Clustering. The correlation analysis of environmental variables with geomorphometric parameters and hydro-environmental variables at a catchment scale indicates a moderate to strong relationship for most attributes, where SRTM exhibits a higher correlation with environmental variables than TanDEM-X. The multiple regression prediction scores for Soil Moisture (R2 = 0.94, RMSE = 0.03), Precipitation Height (R2 = 0.88, RMSE = 86.61), and Drought Index (R2= 0.86, RMSE = 6.83) indicate a strong relationship with geomorphometric parameters and BasinATLAS hydro-environmental variables. The clustering analysis using k-means clustering yield a silhouette score (0.52). The silhouette score and the cross-tabulation table indicate the moderate separation of predicted catchments based on their hydrographic features. Overall, the results indicate favourable workflow compatibility for deriving and incorporating geomorphometric parameters in HydroSHEDS at a catchment scale.

## Research Outline
![alt text](../Plots/Research_Outline.png)

## Aggregation of Geomorphometric Parameters to BasinATLAS (Level 12)

### SRTM
![alt text](../Plots/SRTM_BasinATLAS_Aggregation.png)

### TanDEM-X
![alt text](../Plots/TanDEM-X_BasinATLAS_Aggregation.png)

### DWD Environmental Grids
![alt text](../Plots/DWD_BasinATLAS_Aggregation.png)

## Exploratory Data Analysis (EDA)

## Descriptive Statistics

### Topographic Wetness Index (TWI)
![alt text](../Plots/DS_TWI_SRTM_TDMX.png)

### Terrain Ruggedness Index (TWI)
![alt text](../Plots/DS_TRI_SRTM_TDMX.png)

### Geomorphons
![alt text](../Plots/DS_Geomorphons_SRTM_TDMX.png)

## Correlation and Multiple Regression

### Mean Multi-Annual Soil Moisture with Hydrographic Features
![alt text](../Plots/Correlation_SM.png)
![alt text](../Plots/MR_SM.png)

### Mean Multi-Annual Precipitation Height with Hydrographic Features
![alt text](../Plots/Correlation_PH.png)
![alt text](../Plots/MR_PH.png)

### Mean Multi-Annual Drought Index with Hydrographic Features
![alt text](../Plots/Correlation_DI.png)
![alt text](../Plots/MR_DI.png)

### Clustering (K-Means Clustering)
![alt text](../Plots/Cluster_Distribution.png)
![alt text](../Plots/Cluster_DE_Map.png)