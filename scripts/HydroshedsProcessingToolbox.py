# %% Libraries Import
import matplotlib.pyplot as plt
import geopandas as gpd
from rasterstats import zonal_stats
import rasterio
import os
import numpy as np
import seaborn as sns
from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import KMeans
from sklearn.pipeline import make_pipeline
from sklearn.metrics import silhouette_score
from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split 
from scipy.stats import pearsonr
import matplotlib.pyplot as plt
import pandas as pd

#%% Functions

##################################################
# Zonal Statistics
##################################################
# FOR CALCULATING ZONAL STATISTICS PER CATCHMENT
def zonal_statistics(rasterfile:str, shapefile:str, statsvar:str, col_name:str, plot_title:str, nodataval:int):

    # Obtaining transform information from raster
    affine = rasterfile.transform 

    # Zonal Statistics
    stats = zonal_stats(shapefile, rasterfile.read(1), affine=affine, stats=statsvar, all_touched=True, nodata=nodataval)


    # Empty List to store zonal statistics values
    stats_list = []

    for feature in stats:

        # adding features to the list
        stats_list.append(feature[statsvar])

    # Adding the values to GeoDataFrame
    shapefile[col_name] = stats_list
    
    # Plot
    shapefile.plot(column=col_name, cmap='viridis', legend=True)
        
    # Customize the color scale
    plt.title(plot_title)
    plt.show()

    return shapefile

##################################################
# Mean Raster Calculation
##################################################
def mean_raster(inputfolderpath:str, outputfilepath:str):
    # List all raster files in the input folder
    raster_files = [os.path.join(inputfolderpath, f) for f in os.listdir(inputfolderpath) if f.endswith('.tif')]

    # Empty list to store raster data
    rasters = []

    # Read and Append Rasters
    for raster_file in raster_files:
        with rasterio.open(raster_file) as src:
            rasters.append(src.read(1))  # Read the first band

    # Stack rasters into a numpy array
    raster_stack = np.stack(rasters, axis=0)

    # the mean along the first axis (which represents the stack of rasters)
    mean_raster = np.mean(raster_stack, axis=0)

    # Read metadata 
    with rasterio.open(raster_files[0]) as src:
        meta = src.meta

    # Update the metadata to reflect the number of layers
    meta.update(count=1)

    output_filepath = os.makedirs(outputfilepath, exist_ok=True)

    # Write raster 
    with rasterio.open(output_filepath, 'w', **meta) as dst:
        dst.write(mean_raster, 1)

    # Output Prompt
    print(f"Mean raster written to: {outputfilepath}")

##################################################
# Min-Max Normalization
##################################################
def min_max_normalization(geodataframefile:str, columnname:str):

    # Make a copy of the DataFrame to avoid SettingWithCopyWarning
    geodataframefile = geodataframefile.copy()

    column_to_normalize = columnname

    # Calculate the min and max of the column
    min_value = geodataframefile[column_to_normalize].min()
    max_value = geodataframefile[column_to_normalize].max()

    # Apply Min-Max normalization
    geodataframefile['Normalized_' + column_to_normalize] = (
        geodataframefile[column_to_normalize] - min_value
    ) / (max_value - min_value)

    return geodataframefile

##################################################
# Min-Max Normalization
##################################################
def remove_NA_values(geodataframefile:str):

    # Copy Geodataframe for avoiding errors and warnings
    geodataframefile = geodataframefile.copy()

    # Threshold calculation
    threshold = len(geodataframefile) * 0.05

    # Subsetting the columns
    cols_to_drop = geodataframefile.columns[geodataframefile.isna().sum() <= threshold]
    geodataframefile.dropna(subset = cols_to_drop, inplace = True)

    # Return geodataframe file
    return geodataframefile

##################################################
# Plot Map
##################################################
def plot_map(filename: str, colname: str, plotname: str, plotsize: int = 6, minval: int = None, maxval: int = None, categoricalvalue: bool = None, scale_label: str = None):
    fig, ax = plt.subplots(figsize=(plotsize, plotsize))
    
    # Plot the map with the color bar
    cax = fig.add_axes([0.85, 0.15, 0.03, 0.20])  # Position for the color bar
    map_plot = filename.plot(column=colname, cmap='viridis', legend=True, ax=ax, vmin=minval, vmax=maxval, categorical=categoricalvalue, cax=cax)

    # Labels
    ax.set_xlabel('X-Axis')
    ax.set_ylabel('Y-Axis')
    ax.axis('off')
    
    # Plot title
    ax.set_title(plotname, fontweight='bold', loc='center', pad=20)

    # Set label for the color bar above the scale if provided
    if scale_label:
        cax.set_title(scale_label, pad=10, fontweight='bold', fontsize=10)

    # Show plot
    plt.show()

##################################################
# Pearson Correlation
##################################################
def pearson_correlation(gdf, labellist:list, plottitle:str):

    # Correlation of the GeoDataFrame
    correlation_matrix = gdf.corr(method='pearson')

    # Size of the plot
    plt.figure(figsize=(12, 10))

    # Heatmap for visualisation
    heatmap = sns.heatmap(correlation_matrix, annot=True, cmap='RdYlGn', fmt='.2f', vmin=-1, vmax=1)

    # Custom labels for x and y-axisS
    custom_x_labels = labellist
    custom_y_labels = labellist

    # Custom labels for x and y-axis
    heatmap.set_xticklabels(custom_x_labels, rotation=45, horizontalalignment='right')
    heatmap.set_yticklabels(custom_y_labels, rotation=0, horizontalalignment='right')

    # Title
    plt.title(plottitle,fontweight='bold', loc='center', pad=20)

    # Show plot
    plt.show()

##################################################
# Linear Regression
##################################################
def linear_regression(X, y, plottitle:str, xlabel:str, ylabel:str):

    # Assuming gdf is already defined and contains the columns 'SRTM_TRI' and 'MA_Drought_Index'
    X = X.values
    y = y.values

    # Reshape X to be 2D as required by scikit-learn (n_samples, n_features)
    X = X.reshape(-1, 1)

    # Split the data into training and testing sets
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30, random_state=42)

    # Create the linear regression model
    model = LinearRegression()

    # Fit the model on the training data
    model.fit(X_train, y_train)

    # Predict the values using the model on the test data
    predictions = model.predict(X_test)

    # Plot the test data points
    plt.scatter(X_test, y_test, label='Actual data')

    # Plot the regression line using the test data
    plt.plot(X_test, predictions, color='red', label='Regression line')

    plt.title(plottitle,  fontweight='bold', loc='center', pad=20)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.grid(True)
    plt.legend()
    plt.show()

    # Calculate the R² score using the test data
    r2 = r2_score(y_test, predictions)
    print(f'R² Score on Test Data: {r2}')

    rmse = np.sqrtmean_squared_error(y_true=y_test,y_pred=predictions)
    print(f"Root Mean Square Error: {rmse}")

##################################################
# Multiple Regression
##################################################
def multiple_regression(X, y, plottitle:str, labellist:list): 

    # Splitting the data into training and testing data 
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.30, random_state=42) 

    reg = LinearRegression() 
    reg.fit(X_train, y_train)
    predictions = reg.predict(X_test)

    # Calculate the R² score using the test data
    r2 = r2_score(y_test, predictions)
    rmse = np.sqrt(mean_squared_error(y_test, predictions))

    # Extract Coefficients
    coefficients = reg.coef_

    print(f'R² Score on Test Data: {r2}')
    print(f'Root Mean Square Error: {rmse}')
    
    # Create a DataFrame to display the coefficients with their corresponding feature names
    coeff_df = pd.DataFrame({
        'Feature': X.columns,  
        'Coefficient': coefficients
    })

    new_labels = labellist
    coeff_df['Feature'] = new_labels

    # Sort the DataFrame by absolute value of coefficients
    coeff_df = coeff_df.reindex(coeff_df['Coefficient'].abs().sort_values(ascending=False).index)
    print(coeff_df)
    
    # Normalize the coefficient values and map them to colors using a colormap
    norm = plt.Normalize(vmin=coeff_df['Coefficient'].min(), vmax=coeff_df['Coefficient'].max())
    colormap = plt.cm.coolwarm_r
    colors = colormap(norm(coeff_df['Coefficient'].values))

    # Create a figure and axis for the bar plot
    fig, ax = plt.subplots(figsize=(10, 6))

    # Plot the coefficients with colored bars
    bars = ax.bar(coeff_df['Feature'], coeff_df['Coefficient'], color=colors)
    ax.set_ylabel('Coefficient Values')
    ax.set_title(plottitle, fontweight='bold', loc='center', pad=20)
    ax.set_xticks(range(len(coeff_df['Feature'])))
    ax.set_xticklabels(coeff_df['Feature'], rotation=45, ha='right')
    ax.grid(True)

    # Add color bar for reference
    sm = plt.cm.ScalarMappable(cmap=colormap, norm=norm)
    cbar = fig.colorbar(sm, ax=ax)
    
    plt.tight_layout()
    plt.show()

##################################################
# K-Means Clustering
##################################################
def kmeans_clustering(gdf, attributeslist:list, maptitle:str, crosstabattribute:str, pcacomponents=2, nclusters=4):

    # Subset gdf
    gdf_subset = gdf[attributeslist]

    # Scaling Data
    scaler = StandardScaler()
    gdf_scaled = scaler.fit_transform(gdf_subset)

    # Principal Component Analysis (PCA)
    pca = PCA(n_components=pcacomponents)
    gdf_pca = pca.fit_transform(gdf_scaled)

    # Inertia Plot for determining number of clusters
    ks = range(1, 10)
    inertias = []

    for k in ks:
        # Create a KMeans instance with k clusters: model
        model = KMeans(n_clusters = k, random_state=21)
        
        # Fit model to samples
        model.fit(gdf_pca)
        
        # Append the inertia to the list of inertias
        inertias.append(model.inertia_)
        
    # Plot ks vs inertias
    plt.title('Inertia vs. Number of Clusters (K-Means Elbow Method)',  fontweight='bold', loc='center', pad=20)
    plt.plot(ks, inertias, '-o')
    plt.xlabel('Number of Clusters (k)')
    plt.ylabel('Inertia (Sum of Squared Distances)')
    plt.xticks(ks)
    plt.show()

    # K-Means Clustering
    kmeans = KMeans(n_clusters=nclusters, random_state=21)
    kmeans.fit(gdf_pca)

    # Accquire Centroids and Labels
    centroids = kmeans.cluster_centers_
    labels = kmeans.labels_

    # Plot Clustering Map
    xs = gdf_pca[:,0]
    ys = gdf_pca[:,1]

    centroids_x = centroids[:,0]
    centroids_y = centroids[:,1]

    plt.scatter(xs, ys, cmap='viridis', c=labels)
    plt.scatter(centroids_x, centroids_y, c='red', marker='x', label='Centroids')

    plt.title('K-Means Clustering on PCA-Transformed Features', fontweight='bold', loc='center', pad=20)
    plt.xlabel('Principal Component 1')
    plt.ylabel('Principal Component 2')
    plt.legend()
    plt.show()

    # Add Labels to the GeoDataFrame
    gdf['cluster_labels'] = labels

    
    # Plot CLustering Map
    fig, ax = plt.subplots(figsize=(7, 7))
    gdf.plot(column='cluster_labels', categorical=True, legend=True, cmap='viridis', ax=ax)

    ax.set_title(maptitle, fontweight='bold', loc='center', pad=40)

    # Access the existing legend and customize it
    legend = ax.get_legend()

    # Set the title of the legend
    legend.set_title("Clusters", prop={'size': 10, 'weight': 'bold'})
    legend._legend_box.align = "left"  # Align the text to the left if desired
    legend.get_frame().set_linewidth(1.5)  # Optional: Adjust the frame thickness

    # Adjust legend position to further right and bottom
    legend.set_bbox_to_anchor((1.20, 1))  # Move the legend further to the right
    legend.set_loc('upper right')  # Anchor the legend box's lower right corner to the (1.2, 0) point
    ax.axis('off')

    # Labels
    ax.set_xlabel('X-Axis')
    ax.set_ylabel('Y-Axis')

    plt.show()

    # Cross Tabulation
    true_labels = gdf[crosstabattribute].tolist()
    df = pd.DataFrame({'cluster_labels':labels, 'true_labels': true_labels})

    # Create crosstab: ct
    ct = pd.crosstab(df['cluster_labels'], df['true_labels'])


    # K-Means Clustering Performance Results
    # Silhoutte Score
    score = silhouette_score(gdf_pca, labels)

    # Print Silhouette and Cross Tabulation Scores
    print(f"Silhouette Score: {score}")
    print(ct.to_string())