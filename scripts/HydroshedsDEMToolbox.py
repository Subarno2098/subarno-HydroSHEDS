# %% Libraries Import
import geopandas as gpd
import matplotlib.pyplot as plt
import os
from glob import glob
from tqdm.notebook import tqdm
from rasterstats import zonal_stats
import rasterio
from rasterio.merge import merge
from rasterio.transform import from_origin
from rasterio.enums import Resampling
from rasterio.windows import Window
from rasterio.mask import mask
from rasterio.warp import calculate_default_transform, reproject, Resampling
from shapely.geometry import box
import seaborn as sns
import numpy as np
import math
import importlib
import subprocess
import shutil
from whitebox import WhiteboxTools
wbt = WhiteboxTools()




class HydroshedsDEMToolbox:
    
    def __init__(self, rasterfilepath, folderpath):

        self.rasterfilepath = rasterfilepath
        self.folder_directory = folderpath
        os.makedirs(self.folder_directory, exist_ok=True)

        # initializing Raster, Raster file path and WhiteboxTools instance
        self.raster = rasterio.open(self.rasterfilepath)
        self.wbt = WhiteboxTools()
        self.wbt.set_verbose_mode(False)

    #######################################################################################################################################################################
    # SRTM
    #######################################################################################################################################################################

    ##################################################
    # SRTM SLOPE
    ##################################################

    def SRTM_slope(self, outputfilename:str = 'SRTM_Slope.tif'):

        '''
        This tool calculates slope gradient (i.e. slope steepness in degrees) for each grid cell in an SRTM input digital elevation model (DEM).
        '''
        
        # Output Path   
        self.SRTM_slope_directory  = os.path.join(self.folder_directory, outputfilename)

        # tqdm progress bar
        progress_bar = tqdm(total=1, desc="Processing", dynamic_ncols=True, bar_format="{l_bar}{bar}{r_bar}", colour='green')

        # WBT Slope Calculation
        self.wbt.slope(dem=self.rasterfilepath, output=self.SRTM_slope_directory, units="degrees")

        # Progress Bar Update
        progress_bar.update(1)

        # Progress Bar Close
        progress_bar.close()

        # Output Prompt
        print(f"SLOPE CALCULATION COMPLETE.\nOutput File Name: {outputfilename}\nFolder Path: {self.SRTM_slope_directory}")

    ##################################################
    # SRTM FLOW ACCUMULATION
    ##################################################
    def SRTM_flow_accumulation(self, outputdemfilename:str = 'SRTM_Output_DEM.tif', outputpointerfilename:str = 'SRTM_pntr_DEM.tif', outputflowaccumulationfilename:str = 'SRTM_Flow_Accumulation.tif'):

        '''
        This tool is used to generate a flow accumulation grid (i.e. specific contributing area) for each grid cell in an SRTM input digital elevation model (DEM).
        '''

        # Output Paths
        self.SRTM_output_dem_directory = os.path.join(self.folder_directory, outputdemfilename)
        self.SRTM_pointer_dem_directory = os.path.join(self.folder_directory, outputpointerfilename)
        self.SRTM_flow_accumulation_directory = os.path.join(self.folder_directory, outputflowaccumulationfilename)

        # tqdm progress bar
        progress_bar = tqdm(total= 1, desc="Processing", dynamic_ncols=True, bar_format="{l_bar}{bar}{r_bar}", colour='green')

        # WBT Flow Accumulation
        self.wbt.flow_accumulation_full_workflow(dem=self.rasterfilepath, out_dem=self.SRTM_output_dem_directory, out_pntr=self.SRTM_pointer_dem_directory, out_accum=self.SRTM_flow_accumulation_directory, out_type='specific contributing area')

        # Progress Bar Update
        progress_bar.update(1)

        # Progress Bar Close
        progress_bar.close()

        # Output Prompt
        print(f"FLOW ACCUMULATION CALCULATION COMPLETE.\nOutput File Name: {outputdemfilename}\nFolder Path: {self.SRTM_output_dem_directory}\nOutput File Name: {outputpointerfilename}\nFolder Path: {self.SRTM_pointer_dem_directory}\nOutput File Name: {outputflowaccumulationfilename}\nFolder Path: {self.SRTM_flow_accumulation_directory}")

    ##################################################
    # SRTM D8 FLOW ACCUMULATION
    ##################################################
    def SRTM_D8_flow_accumulation(self, outputfilename:str = 'SRTM_D8_Flow_Accumulation.tif'):

        '''
        This tool is used to generate a flow accumulation grid using D8 Flow Algorithm (i.e. specific contributing area) for each grid cell in an SRTM input digital elevation model (DEM).
        '''

        # Output Path
        self.SRTM_flow_accumulation_directory = os.path.join(self.folder_directory, outputfilename)

        # tqdm progress bar
        progress_bar = tqdm(total= 1, desc="Processing", dynamic_ncols=True, bar_format="{l_bar}{bar}{r_bar}", colour='green')

        # WBT D8 Flow Accumulation
        self.wbt.d8_flow_accumulation(i = self.rasterfilepath, output=self.SRTM_flow_accumulation_directory,out_type='specific contributing area')

        # Progress Bar Update
        progress_bar.update(1)

        # Progress Bar Close
        progress_bar.close()

        # Output Prompt
        print(f"D8 FLOW ACCUMULATION CALCULATION COMPLETE.\nOutput File Name: {outputfilename}\nFolder Path: {self.SRTM_flow_accumulation_directory}")

    ##################################################
    # SRTM WETNESS INDEX
    ##################################################
    def SRTM_wetness_index(self, outputfilename:str = 'SRTM_TWI.tif'):

        '''
        This tool can be used to calculate the topographic wetness index from SRTM DEM, commonly used in the TOPMODEL rainfall-runoff framework. The index describes the propensity for a site to be saturated to the surface given its contributing area and local slope characteristics.
        '''
        
        # Output Path
        self.SRTM_wetness_index_directory = os.path.join(self.folder_directory, outputfilename)
        
        # tqdm progress bar
        progress_bar = tqdm(total= 1, desc="Processing", dynamic_ncols=True, bar_format="{l_bar}{bar}{r_bar}", colour='green')

        # WBT Wetness Index
        self.wbt.wetness_index(sca=self.SRTM_flow_accumulation_directory, slope=self.SRTM_slope_directory, output=self.SRTM_wetness_index_directory)

        # Progress Bar Update
        progress_bar.update(1)

        # Progress Bar Close
        progress_bar.close()

        # Output Prompt
        print(f"TOPOGRAPHIC WETNESS INDEX CALCULATION COMPLETE.\nOutput File Name: {outputfilename}\nFolder Path: {self.SRTM_wetness_index_directory}")

    ##################################################
    # SRTM RUGGEDNESS INDEX
    ##################################################
    def SRTM_ruggedness_index(self, outputfilename:str = 'SRTM_TRI.tif'):

        '''
        The terrain ruggedness index (TRI) is a measure of local topographic relief. The TRI calculates the root-mean-square-deviation (RMSD) for each grid cell in an SRTM digital elevation model (DEM), calculating the residuals (i.e. elevation differences) between a grid cell and its eight neighbours.
        '''

        # tqdm progress bar
        progress_bar = tqdm(total= 1, desc="Processing", dynamic_ncols=True, bar_format="{l_bar}{bar}{r_bar}", colour='green')        
        
        # Output path
        self.SRTM_ruggedness_index_directory = os.path.join(self.folder_directory, outputfilename)
    
        # WBT Ruggedness Index
        self.wbt.ruggedness_index(dem=self.rasterfilepath, output=self.SRTM_ruggedness_index_directory)

        # Progress Bar Update
        progress_bar.update(1)

        # Progress Bar Close
        progress_bar.close()

        # Output Prompt
        print(f"TERRAIN RUGGEDNESS INDEX CALCULATION COMPLETE.\nOutput File Name: {outputfilename}\nFolder Path: {self.SRTM_ruggedness_index_directory}")

    ##################################################
    # SRTM GEOMORPHONS
    ##################################################
    def SRTM_geomorphons(self, outputfilename:str = 'SRTM_Geomorphons.tif'):

        '''
        This tool can be used to perform a geomorphons landform classification based on SRTM digital elevation model. The geomorphons concept is based on line-of-sight analysis for the eight topographic profiles in the cardinal directions surrounding each grid cell in the input DEM.
        '''

        # tqdm progress bar
        progress_bar = tqdm(total= 1, desc="Processing", dynamic_ncols=True, bar_format="{l_bar}{bar}{r_bar}", colour='green')        
         
        # Output path
        self.SRTM_geomorphons_directory = os.path.join(self.folder_directory, outputfilename)

        # WBT Geomorphons
        self.wbt.geomorphons(dem=self.rasterfilepath, output=self.SRTM_geomorphons_directory, search=50, threshold=0.0, fdist=0, skip=0, forms=True, residuals=False)

        # Progress Bar Update
        progress_bar.update(1)

        # Progress Bar Close
        progress_bar.close()

        # Output Prompt
        print(f"GEOMORPHONS CALCULATION COMPLETE.\nOutput File Name: {outputfilename}\nFolder Path: {self.SRTM_geomorphons_directory}")

    ##################################################
    # SRTM FULL WORKFLOW
    ##################################################
    def SRTM_Processing_full_workflow(self):

        '''
        This tool calculates all the geomorphometric parameters (TWI, TRI and Geomorphons) from the SRTM digital elevation model (DEM).
        '''
        
        # tqdm progress bar
        progress_bar = tqdm(total= 5, desc="Processing", dynamic_ncols=True, bar_format="{l_bar}{bar}{r_bar}", colour='green')

        ###################
        # Slope
        ###################

        # Output Path
        SRTM_slope_directory  = os.path.join(self.folder_directory,'SRTM_Slope.tif')

        # WBT Slope
        self.wbt.slope(dem=self.rasterfilepath, output=SRTM_slope_directory, units="degrees")
        
        # Progress Bar Update
        progress_bar.update(1)

        # Output Prompt
        print(f"SLOPE CALCULATION COMPLETE.\nOutput File Name: SRTM_Slope.tif\nFolder Path: {SRTM_slope_directory}")

        ###################
        # Flow accumulation
        ###################
        
        # Output Paths
        SRTM_output_dem_directory = os.path.join(self.folder_directory,'SRTM_Output_DEM.tif')
        SRTM_pointer_dem_directory = os.path.join(self.folder_directory, 'SRTM_pntr_DEM.tif')
        SRTM_flow_accumulation_directory = os.path.join(self.folder_directory, 'SRTM_Flow_Accumulation.tif')

        # WBT Flow Accumulation Full Workflow
        self.wbt.flow_accumulation_full_workflow(dem=self.rasterfilepath, out_dem=SRTM_output_dem_directory,out_pntr=SRTM_pointer_dem_directory, out_accum=SRTM_flow_accumulation_directory, out_type='specific contributing area')

        # Progress Bar Update
        progress_bar.update(1)

        # Output Prompt
        print(f"FLOW ACCUMULATION CALCULATION COMPLETE.\nOutput File Name: SRTM_Output_DEM.tif\nFolder Path: {SRTM_pointer_dem_directory}\nOutput File Name: SRTM_pntr_DEM.tif\nFolder Path: {SRTM_pointer_dem_directory}\nOutput File Name: SRTM_Flow_Accumulation.tif\nFolder Path: {SRTM_flow_accumulation_directory}")

        ###################
        # Wetness Index
        ###################
        
        # Output Path
        SRTM_wetness_index_directory = os.path.join(self.folder_directory, 'SRTM_Wetness_Index.tif')

        # WBT Wetness Index
        self.wbt.wetness_index(sca=SRTM_flow_accumulation_directory, slope=SRTM_slope_directory, output=SRTM_wetness_index_directory)

        # Progress Bar Update
        progress_bar.update(1)

        # Output Prompt
        print(f"TOPOGRAPHIC WETNESS INDEX CALCULATION COMPLETE.\nOutput File Name: SRTM_Wetness_Index.tif\nFolder Path: {SRTM_wetness_index_directory}")

        ###################
        # Ruggedness Index
        ###################

        # Output Path
        SRTM_ruggedness_index_directory = os.path.join(self.folder_directory, 'SRTM_Ruggedness_Index.tif')

        # WBT Ruggedness Index
        self.wbt.ruggedness_index(dem=self.rasterfilepath, output=SRTM_ruggedness_index_directory)

        # Progress Bar Update
        progress_bar.update(1)

        # Output Prompt
        print(f"TERRAIN RUGGEDNESS INDEX CALCULATION COMPLETE.\nOutput File Name: 'SRTM_Ruggedness_Index.tif'\nFolder Path: {SRTM_ruggedness_index_directory}")

        ###################
        # Geomorphons
        ###################

        # Output Path
        SRTM_geomorphons_directory = os.path.join(self.folder_directory, 'SRTM_Geomorphons.tif')

        # WBT Geomorphons
        self.wbt.geomorphons(dem=self.rasterfilepath, output=SRTM_geomorphons_directory, search=50, threshold=0.0, fdist=0, skip=0, forms=True, residuals=False)

        # Progress Bar Update
        progress_bar.update(1)

        # Output Prompt
        print(f"GEOMORPHONS CALCULATION COMPLETE.\nOutput File Name: 'SRTM_Geomorphons.tif'\nFolder Path: {SRTM_geomorphons_directory}")

        ####################
        # Close Progress Bar
        ####################
        progress_bar.close()

    #######################################################################################################################################################################
    # TanDEM-X
    #######################################################################################################################################################################

    ##################################################
    # TanDEM-X Split Raster
    ##################################################
    def TanDEMX_split_raster(self, tilesize:int = 8192, outputfoldername:str = 'TDMX_DEM_SPLIT'):

        '''
        This tool is used for splitting the raster into individual tiles.
        '''


        # Output Path
        self.split_dem_directory = os.path.join(self.folder_directory, outputfoldername)

        # Create Directory
        os.makedirs(self.split_dem_directory, exist_ok=True)

        # Raster Height and Width
        height, width = self.raster.shape

        # To Calculate Number of rows and columns needed for the tiles
        rows = math.ceil(height / tilesize)
        cols = math.ceil(width / tilesize)

        # tqdm progress bar
        progress_bar = tqdm(total= rows * cols, desc="Processing", dynamic_ncols=True, bar_format="{l_bar}{bar}{r_bar}", colour='green')


        # Loop to calculate dimension of each tile and write to disk
        for row in range(rows):
            for col in range(cols):
                # Calculate the window for the current tile
                window = Window(
                    col * tilesize,
                    row * tilesize,
                    min(tilesize, width - col * tilesize), 
                    min(tilesize, height - row * tilesize)
                )

                # Extract data tile
                data = self.raster.read(window=window)  # Read all bands

                # Create a new profile for the tile
                profile = self.raster.profile
                profile.update({
                    'width': min(tilesize, width - col * tilesize),
                    'height': min(tilesize, height - row * tilesize),
                    'transform': self.raster.window_transform(window),
                })

                # Output Tile Path
                output_tile_path = os.path.join(self.split_dem_directory, f'tile_{row}_{col}.tif')

                # Write the tile
                with rasterio.open(output_tile_path, 'w', **profile) as dst:
                    dst.write(data)

                # Progress Bar Update
                progress_bar.update(1)

        # Close the progress bar
        progress_bar.close()

        # Output Prompt
        print(f"SPLIT RASTER COMPLETE.\nOutput Folder Name: {outputfoldername}\nFolder Path: {self.split_dem_directory}")

    ##################################################
    # TanDEM-X Slope
    ##################################################
    def TanDEMX_slope(self, rasterfolderpath=None, outputfoldername='TDMX_SLOPE_SPLIT'):

        '''
        This tool calculates slope gradient (i.e. slope steepness in degrees) for each grid cell in a TanDEM-X input digital elevation model (DEM).
        '''
        if rasterfolderpath is None:
            raster_folder_path = self.split_dem_directory
        else:
            raster_folder_path = rasterfolderpath

        # Output Path
        self.split_slope_path = os.path.join(self.folder_directory, outputfoldername)

        # Create Directory
        os.makedirs(self.split_slope_path, exist_ok=True)

        # List of raster files in the input directory
        raster_files = [f for f in os.listdir(raster_folder_path) if f.endswith('.tif')]

        # tqdm progress bar
        progress_bar = tqdm(total=len(raster_files), desc="Processing", dynamic_ncols=True, bar_format="{l_bar}{bar}{r_bar}", colour='green')

        # Loop through each raster file
        for raster_file in raster_files:
            # Form the full path to the input raster
            input_raster = os.path.join(raster_folder_path, raster_file)

            # Output raster file path
            output_raster = os.path.join(self.split_slope_path, f'slope_{raster_file}')

            # Use WhiteboxTools to calculate slope
            self.wbt.slope(input_raster, output_raster, units="degrees")

            # Update the progress bar
            progress_bar.update(1)

        # Close the progress bar
        progress_bar.close()

        print(f"SLOPE CALCULATION COMPLETE.\nOutput Folder Name: {outputfoldername}\nFolder Path: {self.split_slope_path}")

    ##################################################
    # TanDEM-X Flow Accumulation
    ##################################################
    def TanDEMX_flow_accumulation(self, rasterfolderpath=None, outputdempath:str = "TDMX_Output_DEM_SPLIT", outputpointerdempath:str = "TDMX_pntr_DEM_SPLIT", outputflowaccumulationpath:str = "TDMX_FLOW_ACCUMULATION_SPLIT"):

        '''
        This tool is used to generate a flow accumulation grid (i.e. specific contributing area) for each grid cell in an TanDEM-X input digital elevation model (DEM).
        '''

        if rasterfolderpath is None:
            raster_folder_path = self.split_dem_directory

        else:
            raster_folder_path = rasterfolderpath

        # Join Directories
        self.output_dem_path = os.path.join(self.folder_directory, outputdempath)
        self.pointer_dem_path = os.path.join(self.folder_directory, outputpointerdempath)
        self.flow_accumulation_path  = os.path.join(self.folder_directory, outputflowaccumulationpath)

        # Make Directories if they do not already exist
        os.makedirs(self.output_dem_path, exist_ok=True)
        os.makedirs(self.pointer_dem_path, exist_ok=True)
        os.makedirs(self.flow_accumulation_path, exist_ok=True)

        # List of raster files in the input directory
        raster_files = [f for f in os.listdir(raster_folder_path) if f.endswith('.tif')]

        # tqdm progress bar
        progress_bar = tqdm(total=len(raster_files), desc="Processing", dynamic_ncols=True, bar_format="{l_bar}{bar}{r_bar}", colour='green')

        # Loop through each raster file
        for raster_file in raster_files:
        # Form the full path to the input raster
            input_raster = os.path.join(raster_folder_path, raster_file)

            # Define the output raster file path
            output_raster = os.path.join(self.output_dem_path, f'r_{raster_file}')
            output_pointer = os.path.join(self.pointer_dem_path, f'pt_{raster_file}')
            output_f_acc = os.path.join(self.flow_accumulation_path, f'acc_{raster_file}')

            # Flow Accumulation
            self.wbt.flow_accumulation_full_workflow(dem = input_raster, out_dem = output_raster, out_pntr = output_pointer, out_accum= output_f_acc, out_type='specific contributing area')

            # Update the progress bar
            progress_bar.update(1)

        # Close the progress bar
        progress_bar.close()

        print(f"FLOW ACCUMULATION CALCULATION COMPLETE.\nOutput Folder Name: {outputdempath}\nFolder Path: {self.output_dem_path}\nOutput Pointer Folder Name: {outputpointerdempath}\nFolder Path: {self.pointer_dem_path}\nOutput Flow Accumulation Folder Name: {outputflowaccumulationpath}\nFolder Path: {self.flow_accumulation_path}")

   ##################################################
    # TanDEM-X D8 Flow Accumulation
    ##################################################
    def TanDEMX_D8_flow_accumulation(self, rasterfolderpath=None):

        '''
        This tool is used to generate a flow accumulation grid using D8 Flow Algorithm (i.e. specific contributing area) for each grid cell in a TanDEM-X input digital elevation model (DEM).
        '''

        if rasterfolderpath is None:
            raster_folder_path = self.split_dem_directory

        else:
            raster_folder_path = rasterfolderpath

        # Directory Path
        self.flow_accumulation_path  = os.path.join(self.folder_directory, "TDMX_D8_FLOW_ACCUMULATION_SPLIT")

        # Create Directory 
        os.makedirs(self.flow_accumulation_path, exist_ok=True)

        # List of raster files in the input directory
        raster_files = [f for f in os.listdir(raster_folder_path) if f.endswith('.tif')]

        # tqdm progress bar
        progress_bar = tqdm(total=len(raster_files), desc="Processing", dynamic_ncols=True, bar_format="{l_bar}{bar}{r_bar}", colour='green')

        # Loop through each raster file
        for raster_file in raster_files:
        # Form the full path to the input raster
            input_raster = os.path.join(raster_folder_path, raster_file)

            output_f_acc = os.path.join(self.flow_accumulation_path, f'd8_f_acc_{raster_file}')

            # Flow Accumulation
            self.wbt.d8_flow_accumulation(i=input_raster, output=output_f_acc, out_type='specific contributing area')

            # Update the progress bar
            progress_bar.update(1)

        # Close the progress bar
        progress_bar.close()

        print(f"D8 FLOW ACCUMULATION CALCULATION COMPLETE.\nOutput Folder Name: Split_TDMX_Flow_Accumulation\nFolder Path: {self.flow_accumulation_path}")
        
    ##################################################
    # TanDEM-X Wetness Index
    ##################################################
    def TanDEMX_wetness_index(self, slopefolderpath:str = None, flowaccumulationfolderpath:str = None, outputfoldername:str = 'TDMX_TWI_SPLIT'):

        '''
        This tool can be used to calculate the topographic wetness index from TanDEM-X DEM, commonly used in the TOPMODEL rainfall-runoff framework. The index describes the propensity for a site to be saturated to the surface given its contributing area and local slope characteristics.
        '''

        # Determine slope folder path
        slope_folder_path = slopefolderpath if slopefolderpath is not None else self.split_slope_path

        # Determine flow accumulation path
        flow_accumulation_path = flowaccumulationfolderpath if flowaccumulationfolderpath is not None else self.flow_accumulation_path

        # Create Directory
        self.split_twi_path = os.path.join(self.folder_directory, outputfoldername)

        # Make Directory
        os.makedirs(self.split_twi_path, exist_ok= True)
            
        # list of raster files in the input directories
        slope_files = [f for f in os.listdir(slope_folder_path) if f.endswith('.tif')]
        flow_acc_files = [f for f in os.listdir(flow_accumulation_path) if f.endswith('.tif')]
        
        # tqdm progress bar
        progress_bar = tqdm(total=len(flow_acc_files), desc="Processing", dynamic_ncols=True, bar_format="{l_bar}{bar}{r_bar}", colour='green')

        # Loop for accesing each tile from slope and flow accumulation
        for slope_file, flow_acc_file in zip(slope_files, flow_acc_files):

            # Full path to the input raster file in the loop
            input_raster_slope = os.path.join(slope_folder_path, slope_file)
            input_raster_flow_acc = os.path.join(flow_accumulation_path, flow_acc_file)
            
            # Output path for each tile
            output_wetness_index_tile = os.path.join(self.split_twi_path, f'TWI_{os.path.splitext(slope_file)[0]}_{os.path.splitext(flow_acc_file)[0]}.tif')
        
            # WBT Wetness Index 
            self.wbt.wetness_index(sca=input_raster_flow_acc, slope=input_raster_slope, output=output_wetness_index_tile)

        # Progress Bar Update
            progress_bar.update(1)

        # Progress Bar Close
        progress_bar.close()

        # Output Prompt
        print(f"TOPOGRAPHIC WETNESS INDEX COMPLETE.\nOutput Folder Name: {outputfoldername}\nFolder Path: {self.split_twi_path}")
    
    ##################################################
    # TanDEM-X Ruggedness Index
    ##################################################
    def TanDEMX_ruggedness_index(self, rasterfolderpath=None, outputfoldername='TDMX_TRI_SPLIT'):

        '''
        The terrain ruggedness index (TRI) is a measure of local topographic relief. The TRI calculates the root-mean-square-deviation (RMSD) for each grid cell in a TanDEM-X digital elevation model (DEM), calculating the residuals (i.e. elevation differences) between a grid cell and its eight neighbours.
        '''

        # Determine raster folder path
        raster_folder_path = rasterfolderpath if rasterfolderpath is not None else self.split_dem_directory 

        # Output Directory
        self.split_tri_path = os.path.join(self.folder_directory, outputfoldername)

        # Create Directory
        os.makedirs(self.split_tri_path, exist_ok=True)
            
        # List Raster Files
        raster_files = [f for f in os.listdir(raster_folder_path) if f.endswith('.tif')]
        
        # tqdm progress bar
        progress_bar = tqdm(total=len(raster_files), desc="Processing", dynamic_ncols=True, bar_format="{l_bar}{bar}{r_bar}", colour='green')

        # Loop for accesing each tile from raster files
        for raster_file in raster_files:
            # Full path to the input raster
            input_raster = os.path.join(raster_folder_path, raster_file)

            # Full path output raster file path
            output_ruggedness_index_tile = os.path.join(self.split_tri_path, f'TRI_{raster_file}')

            # WBT Ruggedness Index 
            self.wbt.ruggedness_index(input_raster, output_ruggedness_index_tile)

            # Update the progress bar
            progress_bar.update(1)
        
        # Close Progress Bar
        progress_bar.close()

        print(f"TERRAIN RUGGEDNESS INDEX CALCULATION COMPLETE.\nFolder Name: {outputfoldername}\nFolder Path: {self.split_tri_path}")

    ##################################################
    # TanDEM-X Geomorphons
    ##################################################
    def TanDEMX_geomorphons(self, rasterfolderpath:str = None, outputfoldername:str = 'TDMX_GEOMORPHONS_SPLIT',tilesize = 8192, rowoffset = 2, coloffset= 2):

        '''
        This tool can be used to perform a geomorphons landform classification based on TanDEM-X digital elevation model. The geomorphons concept is based on line-of-sight analysis for the eight topographic profiles in the cardinal directions surrounding each grid cell in the input DEM.
        '''

        if rasterfolderpath is None:
            raster_file = self.raster

        else:
            raster_file = rasterio.open(rasterfolderpath)

        # To Calculate Number of rows and columns needed for the tiles
        height = raster_file.height
        width = raster_file.width
        
        # Calculating number of rows and columns needed for the tiles with offset
        rows = math.ceil(height / (tilesize - rowoffset))
        cols = math.ceil(width / (tilesize - coloffset))

        # Output Directory
        self.split_dem_path_geomorphons = os.path.join(self.folder_directory, 'TDMX_DEM_FOR_GEOMORPHONS_SPLIT')

        # Create Directory
        os.makedirs(self.split_dem_path_geomorphons, exist_ok=True)

        # tqdm Progress Bar
        progress_bar = tqdm(total=rows * cols, desc="Processing", dynamic_ncols=True, bar_format="{l_bar}{bar}{r_bar}", colour='green')

        # Loop to calculate dimension of each tile and write to disk
        for row in range(rows):
            for col in range(cols):
                # Calculate the window for the current tile with offset
                window = Window(
                    col * (tilesize - coloffset),
                    row * (tilesize - rowoffset),
                    min(tilesize, width - col * (tilesize - coloffset)),
                    min(tilesize, height - row * (tilesize - rowoffset))
                )

                # Data for the current tile
                data = raster_file.read(1, window=window)

                # new profile for the tile
                profile = raster_file.profile
                profile.update({
                    'width': tilesize,
                    'height': tilesize,
                    'transform': raster_file.window_transform(window),
                    'count': 1  # assuming a single band raster
                })

                # Ouput Tile Path
                output_tile_path = os.path.join(self.split_dem_path_geomorphons, f'tile_{row}_{col}.tif')

                # Write Tile
                with rasterio.open(output_tile_path, 'w', **profile) as dst:
                    dst.write(data, 1)

                # Progress Bar Update
                progress_bar.update(1)

        # Progress Bar Close
        progress_bar.close()

        # Output Prompt
        print(f"DEM SPLIT FOR GEOMORPHONS COMPLETE.\nFolder Name: TDMX_DEM_FOR_GEOMORPHONS_SPLIT\nFolder Path: {self.split_dem_path_geomorphons}")

        ################################
        # Geomorphons
        ################################

        # Output Directory
        self.split_geomorphons_path = os.path.join(self.folder_directory, outputfoldername)

        # Create Directory
        os.makedirs(self.split_geomorphons_path, exist_ok=True)

        # List of raster files
        raster_files = [f for f in os.listdir(self.split_dem_path_geomorphons) if f.endswith('.tif')]

        # tqdm progress bar
        progress_bar_2 = tqdm(total=len(raster_files), desc="Processing", dynamic_ncols=True, bar_format="{l_bar}{bar}{r_bar}", colour='green')

        # Loop to access each raster file
        for raster_file in raster_files:
            
            # Input Path
            input_raster = os.path.join(self.split_dem_path_geomorphons, raster_file)

            # Output Path
            output_raster = os.path.join(self.split_geomorphons_path, f'geo_m_{raster_file}')

            # WBT Geomorphons
            self.wbt.geomorphons(dem = input_raster, output = output_raster, search=500, threshold=0.0, fdist=0, skip=0, forms=True, residuals=False)

            # Progress Bar Update
            progress_bar_2.update(1)

        # Progress Bar Close
        progress_bar_2.close()

        # Output Prompt
        print(f"GEOMORPHONS CALCULATION COMPLETE.\nFolder Name: {outputfoldername}\nFolder Path: {self.split_geomorphons_path}")

    ##################################################
    # TanDEM-X Merge Rasters
    ##################################################
    def TanDEMX_merge_rasters(self, rasterfolderpath:str, filename:str):

        '''
        This tool is used for merging the split rasters (i.e. Split TWI, Split TRI and Split Geomorphons) into a single raster.
        '''

        # Variable for adding the path
        raster_path = rasterfolderpath

        # Input Path
        input_directory = os.path.join(raster_path, '*.tif')

        # Output Path
        output_directory = os.path.join(self.folder_directory, filename)

        # Globalizing directory for accesing the files
        tiles_glob_path = glob(input_directory)

        # tqdm progress bar
        progress_bar = tqdm(total= len(tiles_glob_path), desc="Processing", dynamic_ncols=True, bar_format="{l_bar}{bar}{r_bar}", colour='green')

        # Looping Raster files
        src_files_to_mosaic = []
        for fp in tiles_glob_path:
            src = rasterio.open(fp)
            src_files_to_mosaic.append(src)

            progress_bar.update(1)
        
        # Merge Raster 
        mosaic, out_trans = merge(src_files_to_mosaic)

        # Metadata for the output mosaic
        out_meta = src_files_to_mosaic[0].meta.copy()
        out_meta.update({
            "driver": "GTiff",
            "height": mosaic.shape[1],
            "width": mosaic.shape[2],
            "transform": out_trans
        })

        # Writing Mosaiced File
        with rasterio.open(output_directory, "w", **out_meta) as dest:
            dest.write(mosaic)

        # Close progress bar
        progress_bar.close()

        # Output Prompt
        print(f"MERGE COMPLETE.\nFile Name: {filename}\nFile Path: {output_directory}")
