# HydroshedsProcessingToolbox Documentation

### Zonal Statistics
### `zonal_statistics`

#### Description
This tool is used to aggregate values of the raster cells to polygons based on provided statistical measure. 

#### Parameters
| Parameter | Type          | Description                                     |
|-----------|---------------|-------------------------------------------------|
|`rasterfile`|`str`|-|
|`shapefile`|`str`|-|
|`statsvar`|`str`|-|
|`col_name`|`str`|-|
|`plot_title`|`str`|-|
|`nodataval`|`int`|-|

#### Syntax
```python
zonal_statistics(rasterfile, shapefile, statsvar, col_name, plot_title, nodataval)
```
#### Example
```python
zonal_statistics(rasterfile=SRTM_TWI, shapefile=BasinATLAS_lvl12_shapefile, statsvar='mean', col_name = 'SRTM_TWI', plot_title = 'SRTM Catchment Based Mean Topographic Wetness Index', nodataval=32767)
```
---
### Mean Raster
### `mean_raster`

#### Description
This tool is used to calculate a mean raster from multiple raster files.

#### Parameters
| Parameter | Type          | Description                                     |
|-----------|---------------|-------------------------------------------------|
|`inputfolderpath`|`str`|Folder path containing the raster files|
|`outputfilepath`|`str`|Output path for writing the calculated mean raster file|

#### Syntax
```python
mean_raster(inputfolderpath, outputfilepath)
```
#### Example
```python
raster_folder = r"D:\Hydrosheds\Data\DWD\EPSG_31467_TIFS\tif_EPSG_31467_multi_annual_soil_moisture_1991_2020"

mean_raster(inputfolderpath=raster_folder, outputfilepath=r"D:\Hydrosheds\Data\DWD\EPSG_31467_TIFS\EPSG_31467_mean_multi_annual_soil_moisture_1991_2020.tif")
```
---

### Min Max Normalization
### `min_max_normalization`

#### Description
This tool is used to normalize the column values based on min max normalization of a geodataframe file.

#### Parameters
| Parameter | Type          | Description                                     |
|-----------|---------------|-------------------------------------------------|
|`geodataframefile`|`str`|Path of the geodataframe file|
|`columnname`|`str`|Column in the geodataframe file|

#### Syntax
```python
min_max_normalization(geodataframefile, columnname)
```
#### Example
```python
min_max_normalization(geodataframefile=modified_BasinATLAS_lvl12, columnname='MA_Soil_Moisture')
```
---
### Remove NA Values
### `remove_NA_values`

#### Description
This tool is used for removal of NA values based on the threshold (<5% NA values in the geodataframe).

#### Parameters
| Parameter | Type          | Description                                     |
|-----------|---------------|-------------------------------------------------|
|`geodataframefile`|`str`|Path of the geodataframe file|

#### Syntax
```python
remove_NA_values(geodataframefile)
```
#### Example
```python
remove_NA_values(geodataframefile=normalized_subset_BasinATLAS_lvl12)
```
### Plotting Maps
### `plot_map`

#### Description
This tool is used for plotting the map from a geodataframe file 

#### Parameters
| Parameter | Type          | Description                                     | Default Value        |
|-----------|---------------|-------------------------------------------------|----------------------|
|`filename`|`str`|Geodataframe file|-|
|`colname`|`str`|Column from the geodataframe for plotting|-|
|`plotname`|`str`|Title of the plot|-|
|`plotsize`|`int`|Size of the plot|*6*|
|`minval`|`int`|Minimum value to be displayed in the plot|*None*|
|`maxval`|`int`|Maximum value to be displayed in the plot|*None*|
|`categoricalvalue`|`bool`|Categorical plot|*None*|
|`scale_label`|`str`|Title of the legend|*None*|

#### Syntax
```python
plot_map(filename, colname, plotname, plotsize, minval, maxval, categoricalvalue, scale_label):
    fig, ax = plt.subplots(figsize=(plotsize, plotsize))
```
#### Example
```python
file_path = r"D:\Hydrosheds\Data\Modified_BasinATLAS_v10_lev12\Subset_Modified_BasinATLAS_lev12.gpkg"
gdf = gpd.read_file(file_path)

plot_map(filename=gdf, colname='SRTM_TWI', plotname='SRTM\nBasinATLAS Mean Topographic Wetness Index', scale_label='TWI')
```
---
### Correlation
### `pearson_correlation`

#### Description
This tool is used to calculate and plot the correlation coefficients based on pearson correlation from the provided geodataframe file.

#### Parameters
| Parameter | Type          | Description                                     |
|-----------|---------------|-------------------------------------------------|
|`gdf`|`GeoDataFrame`|Geodataframe file|
|`labellist`|`list`|To label the parameters of the geodataframe in the correlation plot|
|`plottitle`|`str`|Title of the plot|

#### Syntax
```python
pearson_correlation(gdf, labellist, plottitle)
```
#### Example
```python
subset_list = ['Normalized_MA_Soil_Moisture', 'SRTM_TWI', 'TDMX_TWI', 'SRTM_TRI', 'TDMX_TRI', 'dis_m3_pyr', 'run_mm_syr', 'ria_ha_ssu', 'riv_tc_ssu', 'ele_mt_sav', 'slp_dg_sav', 'tmp_dc_syr', 'pre_mm_syr', 'swc_pc_syr']

gdf_subset = gdf[subset_list]

label_list = ['*Mean Multi-Annual Soil Moisture','SRTM Topographic Wetness Index (TWI)','*TanDEM-X Topographic Wetness Index (TWI)','*SRTM Terrain Ruggedness Index (TRI)','*TanDEM-X Terrain Ruggedness Index (TRI)','Natural Discharge','Land Surface Runoff','River Area','River Volume','Elevation','Terrain Slope','Air Temperature','Precipitation','Soil Water Content']

plot_title = 'Correlation Matrix: Mean-Multi Annual Soil Moisture Vs BasinATLAS Attributes'

pearson_correlation(gdf=gdf_subset, labellist=label_list, plottitle=plot_title)
```
---
### Linear Regression
### `linear_regression`

#### Description
This tool is used for calculating the linear regression of the two selected variables from a geodataframe. 

#### Parameters
| Parameter | Type          | Description                                     |
|-----------|---------------|-------------------------------------------------|
|`X`|`pandas.core.series.Series`|Predictor variable|
|`y`|`pandas.core.series.Series`|Target variable|
|`plottitle`|`str`|Title of the linear regression plot|
|`xlabel`|`str`|Plot label for the predictor variable|
|`ylabel`|`str`|Plot label for the Target variable|

#### Syntax
```python
linear_regression(X, y, plottitle, xlabel, ylabel)
```
#### Example
```python
X = gdf['SRTM_TWI']
y = gdf["Normalized_MA_Soil_Moisture"]
plot_title = "Linear Regression"
xlabel = 'Topographic Wetness Index (TWI)'
ylabel = 'Normalized Soil Moisture'

linear_regression(X=X, y=y, plottitle=plot_title, xlabel=xlabel, ylabel=ylabel)
```
---
### Multiple Regression
### `multiple_regression`

#### Description
This tool is used for calculating the multiple regression of the selected variables from a geodataframe.

#### Parameters
| Parameter | Type          | Description                                     |
|-----------|---------------|-------------------------------------------------|
|`X`|`pandas.core.frame.DataFrame `|Predictor variables from the geodataframe|
|`y`|`pandas.core.series.Series`|Taget variable|
|`plottitle`|`str`|Title of the multiple regression plot|
|`labellist`|`str`|Plot label names for the predictor variables|

#### Syntax
```python
multiple_regression(X=X, y=y, plottitle=plot_title, labellist=label_list)
```
#### Example
```python
X = gdf[['SRTM_TWI', 'TDMX_TWI', 'SRTM_TRI', 'TDMX_TRI', 'dis_m3_pyr', 'run_mm_syr', 'ria_ha_ssu', 'riv_tc_ssu', 'ele_mt_sav', 'slp_dg_sav', 'tmp_dc_syr', 'pre_mm_syr', 'swc_pc_syr']]

y= gdf["Normalized_MA_Soil_Moisture"]

plot_title = "Multiple Regression Coefficients: Mean Multi-Annual Soil Moisture"

label_list = ['*SRTM Topographic Wetness Index (TWI)','*TanDEM-X Topographic Wetness Index (TWI)','*SRTM Terrain Ruggedness Index (TRI)','*TanDEM-X Terrain Ruggedness Index (TRI)','Natural Discharge','Land Surface Runoff','River Area','River Volume','Elevation','Terrain Slope','Air Temperature','Precipitation','Soil Water Content']

multiple_regression(X=X, y=y, plottitle=plot_title, labellist=label_list)
```
---
### K-Means Clustering
### `kmeans_clustering`

#### Description
This tool is used to calculate K-Means clustering from the geodataframe file.

#### Parameters
| Parameter | Type          | Description                                     | Default Value        |
|-----------|---------------|-------------------------------------------------|----------------------|
|`gdf`|`GeoDataFrame`|Name of the output file|-|
|`attributeslist`|`list`|List of variables of the geodataframe to be utilized for clustering|-|
|`maptitle`|`str`|Title of the Map plot|-|
|`crosstabattribute`|`str`|Attribute from the geodataframe to create the cross tabulation table with the existing cluster|-|
|`pcacomponents`|`str`|Number of PCA components (variables from the dataset)|*2*|
|`nclusters`|`str`|Number of clusters for K-Means Clustering|*4*|

#### Syntax
```python
kmeans_clustering(gdf, attributeslist, maptitle, crosstabattribute, pcacomponents, nclusters)
```
#### Example
```python
subset_list = ['SRTM_TWI', 'TDMX_TWI', 'SRTM_TRI', 'TDMX_TRI', 'Normalized_MA_Soil_Moisture', 'MA_Precipitation', 'MA_Drought_Index', 'dis_m3_pyr', 'run_mm_syr', 'ria_ha_ssu', 'riv_tc_ssu', 'ele_mt_sav', 'slp_dg_sav', 'tmp_dc_syr', 'pre_mm_syr', 'swc_pc_syr']
ct_categorical_attribute = 'wet_cl_smj'
plot_title = 'K-Means Clustering of BasinATLAS Attributes'
ct_categorical_attribute = 'wet_cl_smj'

kmeans_clustering(gdf=gdf, attributeslist=subset_list, maptitle=plot_title, crosstabattribute=ct_categorical_attribute, pcacomponents=2, nclusters=4)
```