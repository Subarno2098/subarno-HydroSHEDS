# HydroshedsDEMToolbox Documentation

### `HydroshedsDEMToolbox`

#### Description
It is used to initialize the raster file (DEM) for geomorphometric analysis.

#### Parameters
| Parameter | Type          | Description                                     |
|-----------|---------------|-------------------------------------------------|
| `rasterfilepath`|`str`|To specify the path of the input raster file (.tif)|
| `folderpath`|`str`|To specify the file directory for the output files|

#### Syntax
```python
__init__(self, rasterfilepath, folderpath)
```

## SRTM

#### Initialization Example
```python
dem_path = r"D:\Hydrosheds\Data\DEM\SRTM_ESPG_3395_DEU_DEM.tif"
file_directory = r"D:\Hydrosheds\Outputs\Python\SRTM"

SRTM = HydroshedsDEMToolbox(rasterfilepath=dem_path, folderpath=file_directory)
```

### Slope
### `SRTM_slope`

#### Description
This tool calculates slope gradient (i.e. slope steepness in degrees) for each grid cell in an SRTM input digital elevation model (DEM).

#### Parameters
| Parameter | Type          | Description                                     | Default Value        |
|-----------|---------------|-------------------------------------------------|----------------------|
| `outputfilename`|`str`|To specify the name of the output file|*SRTM_Slope.tif*|

#### Syntax
```python
SRTM_slope(outputfilename)
```
#### Example
```python
SRTM.SRTM_slope()
```
---
### Flow Accumulation
### `SRTM_flow_accumulation`

#### Description
This tool is used to generate a flow accumulation grid (i.e. specific contributing area) for each grid cell in an SRTM input digital elevation model (DEM).

#### Parameters
| Parameter | Type          | Description                                     | Default Value        |
|-----------|---------------|-------------------------------------------------|----------------------|
| `outputdemfilename`|`str`|Output raster DEM file|*SRTM_Output_DEM.tif*|
| `outputpointerfilename`|`str`|Output raster flow pointer file|*SRTM_pntr_DEM.tif*|
| `outputflowaccumulationfilename`|`str`|Output raster flow accumulation file|*SRTM_Flow_Accumulation.tif*|

#### Syntax
```python
SRTM_flow_accumulation(outputdemfilename, outputpointerfilename, outputflowaccumulationfilename)
```
#### Example
```python
SRTM.SRTM_flow_accumulation()
```
---
### D8 Flow Accumulation
### `SRTM_D8_flow_accumulation`

#### Description
This tool is used to generate a flow accumulation grid using D8 Flow Algorithm (i.e. specific contributing area) for each grid cell in an SRTM input digital elevation model (DEM).

#### Parameters
| Parameter | Type          | Description                                     | Default Value        |
|-----------|---------------|-------------------------------------------------|----------------------|
| `outputfilename`|`str`|Output flow accumulation DEM file|*SRTM_D8_Flow_Accumulation.tif*|

#### Syntax
```python
SRTM_D8_flow_accumulation(outputfilename)
```
#### Example
```python
SRTM.SRTM_D8_flow_accumulation()
```
---
### Topographic Wetness Index (TWI)
### `SRTM_wetness_index`

#### Description
This tool can be used to calculate the topographic wetness index from SRTM DEM, commonly used in the TOPMODEL rainfall-runoff framework. The index describes the propensity for a site to be saturated to the surface given its contributing area and local slope characteristics.

#### Parameters
| Parameter | Type          | Description                                     | Default Value        |
|-----------|---------------|-------------------------------------------------|----------------------|
| `outputfilename`|`str`|Output raster DEM file|*SRTM_TWI.tif*|

#### Syntax
```python
SRTM_wetness_index(outputfilename)
```
#### Example
```python
SRTM.SRTM_wetness_index()
```
---
### Terrain Ruggedness Index (TRI)
### `SRTM_ruggedness_index`

#### Description
The terrain ruggedness index (TRI) is a measure of local topographic relief. The TRI calculates the root-mean-square-deviation (RMSD) for each grid cell in an SRTM digital elevation model (DEM), calculating the residuals (i.e. elevation differences) between a grid cell and its eight neighbours.

#### Parameters
| Parameter | Type          | Description                                     | Default Value        |
|-----------|---------------|-------------------------------------------------|----------------------|
| `outputfilename`|`str`|Output raster DEM file|*SRTM_TRI.tif*|

#### Syntax
```python
SRTM_ruggedness_index(outputfilename)
```
#### Example
```python
SRTM.SRTM_ruggedness_index()
```
---
### Geomorphons
### `SRTM_geomorphons`

#### Description
This tool can be used to perform a geomorphons landform classification based on SRTM digital elevation model. The geomorphons concept is based on line-of-sight analysis for the eight topographic profiles in the cardinal directions surrounding each grid cell in the input DEM.

#### Parameters
| Parameter | Type          | Description                                     | Default Value        |
|-----------|---------------|-------------------------------------------------|----------------------|
| `outputfilename`|`str`|Output raster DEM file|*SRTM_Geomorphons.tif*|

#### Syntax
```python
SRTM_geomorphons(outputfilename)
```
#### Example
```python
SRTM.SRTM_geomorphons()
```
---
### SRTM Complete Workflow
### `SRTM_Processing_full_workflow`

#### Description
This tool calculates all the geomorphometric parameters (TWI, TRI and Geomorphons) from the SRTM digital elevation model (DEM).

#### Syntax
```python
SRTM_Processing_full_workflow(self)
```
#### Example
```python
SRTM.SRTM_Processing_full_workflow()
```
---
## TanDEM-X

#### Initialization Example
```python
dem_path = r"D:\Hydrosheds\Data\DEM\TDMX_EPSG_3395_DEU_DEM.tif"
file_directory = r"D:\Hydrosheds\Outputs\Python\TanDEM-X"

TDMX = HydroshedsDEMToolbox(rasterfilepath=dem_path, folderpath=file_directory)
```

### Split Raster Digital Elevation Model (DEM)
### `TanDEMX_split_raster`

#### Description
This tool is used for splitting the raster into individual tiles.
#### Parameters
| Parameter | Type          | Description                                     | Default Value        |
|-----------|---------------|-------------------------------------------------|----------------------|
| `tilesize`|`int`|Tile size in pixels for each tile|*8192*|
| `outputfoldername`|`str`|Folder name and path for split raster tiles|*TDMX_DEM_SPLIT*|

#### Syntax
```python
TanDEMX_split_raster(tilesize, outputfoldername)
```
#### Example
```python
TDMX.TanDEMX_split_raster()
```
---
### Slope
### `TanDEMX_slope`

#### Description
This tool calculates slope gradient (i.e. slope steepness in degrees) for each grid cell in a TanDEM-X input digital elevation model (DEM).

#### Parameters
| Parameter | Type          | Description                                     | Default Value        |
|-----------|---------------|-------------------------------------------------|----------------------|
| `rasterfolderpath`|`None`|To specify the path of the input raster tiles|*None*|
| `outputfoldername`|`str`|To specify the path of the output slope tiles|*TDMX_SLOPE_SPLIT*|

#### Syntax
```python
TanDEMX_slope()
```
or
```python
TanDEMX_slope(rasterfolderpath=None, outputfoldername='TDMX_SLOPE_SPLIT')
```
#### Example
```python
TDMX.TanDEMX_slope()
```
---
### Flow Accumulation
### `TanDEMX_flow_accumulation`

#### Description
This tool is used to generate a flow accumulation grid (i.e. specific contributing area) for each grid cell in an TanDEM-X input digital elevation model (DEM).

#### Parameters
| Parameter | Type          | Description                                     | Default Value        |
|-----------|---------------|-------------------------------------------------|----------------------|
| `rasterfolderpath`|`None`|To specify the path of the input raster tiles|*None*|
| `outputdempath`|`str`|Output raster DEM folder path|*TDMX_Output_DEM_SPLIT*|
| `outputpointerdempath`|`str`|Output raster flow pointer folder path|*TDMX_pntr_DEM_SPLIT*|
| `outputflowaccumulationpath`|`str`|Output raster flow accumulation folder path|*TDMX_FLOW_ACCUMULATION_SPLIT*|

#### Syntax
```python
TanDEMX_flow_accumulation()
```
or
```python
TanDEMX_flow_accumulation(rasterfolderpath=None, outputdempath:str = "TDMX_Output_DEM_SPLIT", outputpointerdempath:str = "TDMX_pntr_DEM_SPLIT", outputflowaccumulationpath:str = "TDMX_FLOW_ACCUMULATION_SPLIT")
```
#### Example
```python
TDMX.TanDEMX_flow_accumulation()
```
---
### D8 Flow Accumulation
### `TanDEMX_D8_flow_accumulation`

#### Description
This tool is used to generate a flow accumulation grid using D8 Flow Algorithm (i.e. specific contributing area) for each grid cell in a TanDEM-X input digital elevation model (DEM).

#### Parameters
| Parameter | Type          | Description                                     | Default Value        |
|-----------|---------------|-------------------------------------------------|----------------------|
| `rasterfolderpath`|`str`|Input raster tiles folder path|*None*|

#### Syntax
```python
TanDEMX_D8_flow_accumulation()
```
or
```python
TanDEMX_D8_flow_accumulation(rasterfolderpath)
```
#### Example
```python
TDMX.TanDEMX_D8_flow_accumulation()
```
---
### Topographic Wetness Index
### `TanDEMX_wetness_index`

#### Description
This tool can be used to calculate the topographic wetness index from TanDEM-X DEM, commonly used in the TOPMODEL rainfall-runoff framework. The index describes the propensity for a site to be saturated to the surface given its contributing area and local slope characteristics.

#### Parameters
| Parameter | Type          | Description                                     | Default Value        |
|-----------|---------------|-------------------------------------------------|----------------------|
| `slopefolderpath`|`str`|Input slope raster folder path|*None*|
| `flowaccumulationfolderpath`|`str`|Input flow accumulation folder path|*None*|
| `outputfoldername`|`str`|Output folder name|*TDMX_TWI_SPLIT*|

#### Syntax
```python
TanDEMX_wetness_index()
```
or
```python
TanDEMX_wetness_index(slopefolderpath, flowaccumulationfolderpath, outputfoldername)
```
#### Example
```python
TDMX.TanDEMX_wetness_index()
```
---
### Terrain Ruggedness Index (TRI)
### `TanDEMX_ruggedness_index`

#### Description
The terrain ruggedness index (TRI) is a measure of local topographic relief. The TRI calculates the root-mean-square-deviation (RMSD) for each grid cell in a TanDEM-X digital elevation model (DEM), calculating the residuals (i.e. elevation differences) between a grid cell and its eight neighbours.

#### Parameters
| Parameter | Type          | Description                                     | Default Value        |
|-----------|---------------|-------------------------------------------------|----------------------|
| `rasterfolderpath`|`str`|Raster tiles folder path|*None*|
| `outputfoldername`|`str`|Output Folder Name|*TDMX_TRI_SPLIT*|

#### Syntax
```python
TanDEMX_ruggedness_index()
```
or
```python
TanDEMX_ruggedness_index(rasterfolderpath, outputfoldername)
```
#### Example
```python
TDMX.TanDEMX_ruggedness_index()
```
---
### Geomorphons
### `TanDEMX_geomorphons`

#### Description
This tool can be used to perform a geomorphons landform classification based on TanDEM-X digital elevation model. The geomorphons concept is based on line-of-sight analysis for the eight topographic profiles in the cardinal directions surrounding each grid cell in the input DEM.

#### Parameters
| Parameter | Type          | Description                                     | Default Value        |
|-----------|---------------|-------------------------------------------------|----------------------|
| `rasterfolderpath`|`str`|Raster tiles folder path|*SRTM_Geomorphons.tif*|
| `outputfoldername`|`str`|Output Folder Name|*TDMX_GEOMORPHONS_SPLIT*|
| `tilesize`|`int`|Tile size in pixels for each tile|*8192*|
| `rowoffset`|`int`|Row offset pixel size|*2*|
| `coloffset`|`int`|Column offset pixel size|*2*|

#### Syntax
```python
TanDEMX_geomorphons()
```
or
```python
TanDEMX_geomorphons(rasterfolderpath, outputfoldername, tilesize, rowoffset, coloffset)
```
#### Example
```python
TDMX.TanDEMX_geomorphons()
```
---
### Merge Rasters
### `TanDEMX_merge_rasters`

#### Description
This tool is used for merging the split rasters (i.e. Split TWI, Split TRI and Split Geomorphons) into a single raster.

#### Parameters
| Parameter | Type          | Description                                     | Default Value        |
|-----------|---------------|-------------------------------------------------|----------------------|
| `rasterfolderpath`|`str`|Input Raster tiles folder path|*-*|
| `filename`|`str`|Output file name|*-*|

#### Syntax
```python
TanDEMX_merge_rasters(rasterfolderpath, filename)
```
#### Example
```python
TDMX.TanDEMX_merge_rasters(rasterfolderpath=r"D:\Hydrosheds\Outputs\Python\TanDEM-X\TDMX_TWI_SPLIT", filename="TDMX_TWI.tif")
```
---